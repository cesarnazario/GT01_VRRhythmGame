using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saber : MonoBehaviour
{

    [SerializeField]
    private float _maxDistance;
    private Vector3 _lastPos;
    public Colors HandleColor;
    [SerializeField]
    private float _rotationAngle;


    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, _maxDistance))
        {
            var cube = hit.collider.GetComponent<Cube>();
            if (cube != null)
            {
                if (Vector3.Angle(transform.position - _lastPos, hit.transform.up) > _rotationAngle && HandleColor == cube.CubeColor)
                {
                    GameManager.Score += 100 * GameManager.Combo;
                    GameManager.Fails = 0;
                }
                Destroy(hit.transform.gameObject);
            }
        }
        _lastPos = transform.position;
    }
}
