using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    private Cube[] _cubes;
    [SerializeField]
    private Obstacle[] _obstacles;
    [SerializeField]
    private Transform[] _spawnPos;

    [SerializeField]
    private AudioSource _audioSource;
    // files from https://motionarray.com/royalty-free-music/hip-hop-rock-cinematic-trailer-29865/
    //https://motionarray.com/royalty-free-music/urban-hip-hop-boom-bap-47600/

    public SongMap _jsonData = new SongMap();
    public TextAsset JSONFile;

    private int _currentNote = 0;
    private int _currentObstacles = 0;
    private Transform _previousPos;
    private Cube _previousCube;

    private void Awake()
    {
        _jsonData = JsonUtility.FromJson<SongMap>(JSONFile.text);
    }

    private void FixedUpdate()
    {
        ReadSongMap();
    }

    private void ReadSongMap()
    {
        if (_audioSource.isPlaying == false) return;
        if (_audioSource.time >= _jsonData.Notes[_currentNote]._time)
        {
            SpawnCube();
            _currentNote++;
        }
        if (_audioSource.time >= _jsonData.Obstacles[_currentObstacles]._time)
        {
            SpawnObstacles();
            _currentObstacles++;
        }
    }

    private void SpawnObstacles()
    {

        var clone = Instantiate(GetObstacle(), GetObstaclePos().position, Quaternion.identity);
        clone.transform.localScale = new Vector3(clone.transform.localScale.x, clone.transform.localScale.y, _jsonData.Obstacles[_currentObstacles]._width);

    }

    private void SpawnCube()
    {
        var clone = Instantiate(GetCube(), GetPos().position, Quaternion.identity);
        clone.SetRotation(_jsonData.Notes[_currentNote]._cutDirection);
    }


    private Transform GetPos()
    {
        var newPos = _spawnPos[_jsonData.Notes[_currentNote]._lineIndex];
        return newPos;
    }

    private Cube GetCube()
    {
        var newCube = _cubes[_jsonData.Notes[_currentNote]._type];
        return newCube;
    }

    private Obstacle GetObstacle()
    {
        var newObstacle = _obstacles[_jsonData.Obstacles[_currentObstacles]._type];
        return newObstacle;
    }
    private Transform GetObstaclePos()
    {
        var newPos = _spawnPos[_jsonData.Obstacles[_currentObstacles]._lineIndex];
        return newPos;
    }
    public void Fail()
    {
        _audioSource.Stop();
    }
}
