using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Colors { red, blue }
[RequireComponent(typeof(Rigidbody))]
public class Cube : MonoBehaviour
{

    private Rigidbody _rigidBody;
    [SerializeField]
    private GameObject _sphere;
    public Colors CubeColor;

    private float[] _rotations =
        {
        0,
        180,
        0,
        0,
        90,
        270,
        0,
        0,
        0
    };

    [SerializeField]
    private float _speed = 15f;

    // Start is called before the first frame update
    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _rigidBody.velocity = -transform.right * _speed;
    }

    public void SetRotation(int rotationIndex)
    {
        transform.Rotate(_rotations[rotationIndex], transform.rotation.y, transform.rotation.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        var destroy = other.GetComponent<DestroyLimit>();
        var combo = other.GetComponent<ComboLimit>();

        if (destroy != null)
        {
            Destroy(this.gameObject);
            GameManager.Fails++;

        }
        if (combo != null)
        {
            GameManager.Combo = 1;


        }
    }
}
