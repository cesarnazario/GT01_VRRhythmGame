using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Notes
{
    public float _time;
    public int _lineIndex;
    public int _lineLayer;
    public int _type;
    public int _cutDirection;


}
