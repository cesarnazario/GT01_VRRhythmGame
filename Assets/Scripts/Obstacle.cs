using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Obstacle : MonoBehaviour
{
    private Rigidbody _rigidBody;
    [SerializeField]
    private float _speed = 15f;

    void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _rigidBody.velocity = -transform.right * _speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<Player>();
        if (player != null)
        {
            GameManager.Combo = 1;
        }
    }

}
