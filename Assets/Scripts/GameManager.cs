using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int Score = 0;
    public static int Combo = 1;
    public static int ComboFactor = 0;
    public static int Fails = 0;
    [SerializeField]
    private GameObject _inGameScreen;
    [SerializeField]
    private GameObject _loosingScreen;
    private TMP_Text _scoreDisplay;
    private TMP_Text _comboDisplay;
    [SerializeField]
    private AudioManager _audioManager;

    private void Start()
    {
        Score = 0;
        ComboFactor = 1;
        Fails = 0;
        Combo = 0;
        if (_loosingScreen != null) _loosingScreen.SetActive(false);

        if (_inGameScreen != null)
        {
            var textDisplay = _inGameScreen.GetComponentsInChildren<TMP_Text>();
            _scoreDisplay = textDisplay[0];
            _comboDisplay = textDisplay[1];
        }
    }

    private void Update()
    {
        if (ComboFactor >= 10)
        {
            Combo++;
            ComboFactor = 0;
        }
        if (_scoreDisplay != null) _scoreDisplay.text = $"Score: {Score}";
        if (_comboDisplay != null) _comboDisplay.text = $"x: {Combo}";
        if (Fails > 10) { GameOver(); }
    }

    private void GameOver()
    {
        _audioManager.Fail();
        _loosingScreen.SetActive(true);
    }
    public void Play(string sceneName)
    {

        SceneManager.LoadScene(sceneName);

    }
}